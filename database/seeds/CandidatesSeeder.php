<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('candidates')->insert([
            'id' => null,
            'name' => Str::random(20),
            'email' => Str::random(20).'@gmail.com',
            'created_at' =>carbon::now(),
            'updated_at' =>carbon::now(),
        ]);
    }
}