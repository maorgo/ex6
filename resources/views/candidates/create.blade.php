@extends('layouts.app')

@section ('title' , 'Create candidates')

@section('content')


                        <h1>Create Candidate</h1>
                        <form method = "post" action= "{{action('CandidatesController@store')}}">
                        @csrf 
                        <div class ="form-group">
                            <label for = "name">Candidate name</label>
                            <input type= "text" class= "form-control" name="name">
                        </div>    
                        <div class ="form-group">
                            <label for = "email">Candidate email</label>
                            <input type= "text" class= "form-control" name="email">
                        </div>  
                        <div>
                            
                            <button type="submit" class="btn btn-primary">Create candidate</button>
                        </div>  
                        </form>  
@endsection        

